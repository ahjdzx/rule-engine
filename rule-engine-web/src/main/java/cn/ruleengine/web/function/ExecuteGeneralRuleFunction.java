package cn.ruleengine.web.function;

import cn.ruleengine.core.*;
import cn.ruleengine.core.annotation.Executor;
import cn.ruleengine.core.annotation.FailureStrategy;
import cn.ruleengine.core.annotation.Function;
import cn.ruleengine.core.annotation.Param;
import cn.ruleengine.core.exception.FunctionException;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 * 执行规则函数，当满足指定条件时可以触发此规则执行所需规则
 *
 * @author dingqianwen
 * @date 2020/8/18
 * @since 1.0.0
 */
@Slf4j
@Function
public class ExecuteGeneralRuleFunction {

    @Resource
    private GeneralRuleEngine generalRuleEngine;

    /**
     * 执行规则函数方法
     *
     * @param workspaceCode 工作空间code
     * @param ruleCode      规则code
     * @param param         所有元素
     * @return 规则返回值
     */
    @Executor
    public Object executor(@Param String workspaceCode, @Param String ruleCode, Map<String, Object> param) {
        log.info("规则执行函数入参：{}-{}-{}", workspaceCode, ruleCode, param);
        if (!this.generalRuleEngine.isExists(workspaceCode, ruleCode)) {
            throw new FunctionException("规则在引擎中不存在：{}", ruleCode);
        }
        // 规则入参
        Input input = new DefaultInput(param);
        // 执行规则
        Output execute = this.generalRuleEngine.execute(input, workspaceCode, ruleCode);
        return execute.getValue();
    }

    @FailureStrategy
    public Object failureStrategy(@Param String workspaceCode, @Param String ruleCode) {
        log.warn("函数执行失败:{}-{} 返回默认值null", workspaceCode, ruleCode);
        return null;
    }


}
